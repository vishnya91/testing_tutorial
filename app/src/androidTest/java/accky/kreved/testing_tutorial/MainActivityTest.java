package accky.kreved.testing_tutorial;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Mock private SampleClass mockSample;

    @Rule public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void listGoesOverTheFold() {
        onView(withText(R.string.hello_world)).check(matches(isDisplayed()));
    }

    @Test
    public void mockitoUnitTest() {
        mockSample.SampleSumMethod(1, 2);
        verify(mockSample, times(1)).SampleSumMethod(1, 2);
    }

    @Test
    public void testButtonClick() throws Exception {
        onView(withId(R.id.push_me_button)).perform(click());
        onView(withId(R.id.txt)).check(matches(withText("true")));
        onView(withId(R.id.push_me_button)).perform(click());
        onView(withId(R.id.txt)).check(matches(withText("false")));
        onView(withId(R.id.push_me_button)).perform(click());
        onView(withId(R.id.txt)).check(matches(withText("true")));
    }
}
