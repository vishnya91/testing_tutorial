package accky.kreved.testing_tutorial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject SampleInterface sampleInstance;

    private boolean flag = false;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.txt);

        ((SuperApplication)getApplication()).getComponent().inject(this);
        sampleInstance.SampleSumMethod(2, 3);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onButtonClick(View view) {
        flag = !flag;
        textView.setText(flag ? "true" : "false");
    }

    public void onToastButtonClick(View view) {
        Toast.makeText(this, R.string.test_toast, Toast.LENGTH_SHORT).show();
    }
}
