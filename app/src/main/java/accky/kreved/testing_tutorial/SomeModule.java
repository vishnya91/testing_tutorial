package accky.kreved.testing_tutorial;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SomeModule {

    @Provides @Singleton
    SampleInterface provideSampleInstance() {
        return new SampleClass();
    }

}
