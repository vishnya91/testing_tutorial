import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import accky.kreved.testing_tutorial.SampleClass;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

//@RunWith(MockitoJUnitRunner.class)
public class MockitoTests {
    @Mock private SampleClass mockSample;
    @Spy private SampleClass spySample;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    public void testFirst() throws Exception {
        int r = mockSample.SampleSumMethod(1, 2);
        System.out.println("mock test: " + r);
        verify(mockSample, times(1)).SampleSumMethod(1, 2);
    }

    @Test
    public void testStubbing() throws Exception {
        when(mockSample.SampleSumMethod(1, 2)).thenReturn(2);
        int r = mockSample.SampleSumMethod(1, 2);
        System.out.println("stub test: " + r);
        verify(mockSample, times(1)).SampleSumMethod(1, 2);
    }

    @Test
    public void testSecond() throws Exception {
        int r = spySample.SampleSumMethod(1, 2);
        System.out.println("spy test: " + r);
        verify(spySample, times(1)).SampleSumMethod(1, 2);
    }



    @Test
    public void testDoIf() throws Exception {
        spySample.doIf(true);
        verify(spySample).doSmth();
    }

    @Test
    public void testNotDoIf() throws Exception {
        spySample.doIf(false);
        verify(spySample, never()).doSmth();
    }
}
